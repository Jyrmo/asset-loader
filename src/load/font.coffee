import { uniqueId } from 'lodash'

import Font from '@jyrmo/bmp-font'

import loadImage from './image'
import loadText from './text'

type = 'font'
namePrefix = 'fnt_'

export default (params, name = uniqueId namePrefix) ->
  { fntUrl, pngUrl } = params
  [imgAssetSpec, textAssetSpec] = await Promise.all [
    loadImage { src: pngUrl }
    loadText { url: fntUrl }
  ]
  font = new Font imgAssetSpec.asset, textAssetSpec.asset
  { type, asset: font, name }
