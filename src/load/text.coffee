import { uniqueId } from 'lodash'

type = 'text'
namePrefix = 'txt_'
method = 'GET'

export default (params, name = uniqueId namePrefix) ->
  { url } = params
  new Promise (resolve, reject) ->
    req = new XMLHttpRequest()
    req.open method, url
    # TODO: allow for req.setRequestHeader contentType
    req.onload = ->
      # TODO: account for different HTTP response codes
      res = { type, asset: req.responseText, name }
      resolve res
    req.onerror = reject
    req.send()