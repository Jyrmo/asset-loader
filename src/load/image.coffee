import { uniqueId } from 'lodash'

type = 'image'
namePrefix = 'img_'

export default (params, name = uniqueId namePrefix) ->
  { src } = params
  new Promise (resolve, reject) ->
    img = new Image()
    res = { type, asset: img, name }
    img.onload = -> resolve res
    img.onerror = reject
    img.src = src
