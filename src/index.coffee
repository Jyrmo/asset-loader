import * as load from './load'

# TODO: possibly a class
# TODO: allow for dynamic adding of loaders

export assets = {
  image: {}
  text: {}
  font: {}
}

# spec is { type, name, params }
export loadSpec = (spec) ->
  # TODO: error on invalid type
  { type, name, params } = spec
  { asset, name } = await load[type] params, name
  # TODO: catch errors from await?
  # TODO: error if duplicate
  assets[type][name] = asset
  { type, name, asset }

export loadSpecs = (specs) ->
  promises = (loadSpec spec for spec in specs)
  Promise.all promises

# TODO: error if no such asset
export getAsset = (type, name) -> assets[type][name]

export getImage = (name) -> getAsset 'image', name

export getText = (name) -> getAsset 'text', name

export getFont = (name) -> getAsset 'font', name

# TODO: unload