require('babel-polyfill')

const loader = require('../lib/index')

// TODO: test invalid files

const imgSpec = {
  type: 'image',
  name: 'arrow-up',
  params: {
    src: 'kb-arrow-up.png'
  }
}

const textSpec = {
  type: 'text',
  name: 'texty',
  params: {
    url: 'text.txt'
  }
}

const fontSpec = {
  type: 'font',
  params: {
    fntUrl: 'Consolas-72.fnt',
    pngUrl: 'Consolas-72.png'
  }
}

loader.loadSpecs([imgSpec, textSpec, fontSpec]).then((results) => {
  console.log(results)
  console.log(loader.assets)
  console.log(loader.getAsset('font', 'fnt_1'))
  console.log(loader.getText('texty'))
})